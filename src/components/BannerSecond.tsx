import { PlayCircle } from '@/components/icons/PlayCircle';

export const BannerSecond = () => {
  return (
    <section className="banner-second flex pt-8 px-3 mt-6">
      <div className="container mt-auto relative">
        <div className="row flex-row-reverse py-7">
          <div className="col-12 col-md-6 relative z-1 self-center text-right text-center-md px-6">
            <h1 className="fw-800 text-primary">
              One more friend
              <small className="fw-700 block">Thousands more fun!</small>
            </h1>
            <p className="my-6 fw-500">
              Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!
            </p>

            <div className="flex group">
              <a href="#" className="btn btn-outline-primary rounded-pill mr-2">
                View Intro
                <PlayCircle className="ml-2" />
              </a>
              <a href="#" className="btn btn-primary rounded-pill ml-2">
                Explore Now
              </a>
            </div>
          </div>

          <div className="col-12 col-md-6 text-center relative z-1">
            <img
              loading="lazy"
              decoding="async"
              alt="With dog"
              src="/media/img/girl_kissing_puppy.png"
              className="relative"
              style={{ bottom: -26 }}
            />
          </div>          
        </div>

        <div className="gray-bg pe-none" />
      </div>
    </section>
  );
}
