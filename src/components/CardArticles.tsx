import { ArrowRight } from '@/components/icons/ArrowRight';

type IArticle = {
  id: string;
  title: string;
  desc: string;
  category: string;
  thumbnail: string;
}

export const CardArticles = () => {
  const ARTICLES: Array<IArticle> = [
    {
      id: "1",
      title: "What is a Pomeranian? How to Identify Pomeranian Dogs",
      desc: "The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed.",
      category: "Pet knowledge",
      thumbnail: "/media/img/article_1.png"
    },
    {
      id: "2",
      title: "Dog Diet You Need To Know",
      desc: "Dividing a dog's diet may seem simple at first, but there are some rules you should know so that your dog can easily absorb the nutrients in the diet. For those who are just starting to raise dogs, especially newborn puppies with relatively weak resistance.",
      category: "Pet knowledge",
      thumbnail: "/media/img/article_2.png"
    },
    {
      id: "3",
      title: "Why Dogs Bite and Destroy Furniture and How to Prevent It Effectively",
      desc: "Dog bites are common during development. However, no one wants to see their furniture or important items being bitten by a dog.",
      category: "Pet knowledge",
      thumbnail: "/media/img/article_3.png"
    },
  ];

  return (
    <section className="py-7 mb-6">
      <div className="container">
        <div className="flex flex-wrap items-center my-6 px-3">
          <h1 className="text-2xl fw-700">
            <small className="block text-base fw-500">You already know ?</small>
            Useful pet knowledge
          </h1>

          <a href="#" className="btn btn-outline-primary rounded-pill ml-auto hide-xs">
            View more
            <ArrowRight className="ml-2" />
          </a>
        </div>

        <div className="row gap-y-4">
          {ARTICLES.map((item: IArticle) =>
            <article key={item.id} className="col-12 col-md-4">
              <div className="card h-full shadow-sm">
                <a href="#" className="no-underline">
                  <img
                    loading="lazy"
                    decoding="async"
                    alt={item.title}
                    src={item.thumbnail}
                    className="w-full rounded-10px"
                  />
                </a>
                <div className="card-body">
                  <a href="#" className="no-underline badge">
                    {item.category}
                  </a>
                  <h1 className="text-base mb-0 mt-2">
                    <a href="#" className="no-underline text-dark-1 fw-700">{item.title}</a>
                  </h1>
                  <div className="my-1 text-sm text-gray">
                    {item.desc}
                  </div>
                </div>
              </div>
            </article>
          )}
        </div>

        <div className="px-3 hide-md">
          <a href="#" className="btn btn-outline-primary rounded-pill w-full mt-4">
            View more
            <ArrowRight className="ml-2" />
          </a>
        </div>
      </div>
    </section>
  );
}
