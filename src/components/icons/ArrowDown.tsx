export const ArrowDown = (props: any) => (
  <svg {...props} width="8" height="5" viewBox="0 0 8 5" fill="none">
    <path 
      d="M7 1L4 4L1 1" 
      stroke="#002A48" 
      strokeWidth="2" 
      strokeLinecap="round" 
      strokeLinejoin="round"
    />
  </svg>
);
