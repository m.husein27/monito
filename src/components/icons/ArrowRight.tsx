export const ArrowRight = (props: any) => (
  <svg {...props} width="6" height="10" viewBox="0 0 6 10" fill="none">
    <path 
      d="M1.33337 1.66666L4.66671 4.99999L1.33337 8.33332" 
      stroke="currentColor" // #003459
      strokeWidth="1.5" 
      strokeLinecap="round" 
      strokeLinejoin="round"
    />
  </svg>
);
