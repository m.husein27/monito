import { useState, useEffect, useRef } from 'react';
import { Search } from '@/components/icons/Search';
import { StarCircleRed } from '@/components/icons/StarCircleRed';
import { ArrowDown } from '@/components/icons/ArrowDown';
import { Close } from '@/components/icons/Close';

export const Header = () => {
  const inputSearchRef: any = useRef();
  const [sticky, setSticky] = useState<boolean>(false);
  const [openOffcanvas, setOpenOffcanvas] = useState<boolean>(false);
  const [openFormSearch, setOpenFormSearch] = useState<boolean>(false);

  useEffect(() => {
    const onScroll = (e: any) => {
      e.stopPropagation();
      const scrollY = window.scrollY || document.documentElement.scrollTop;
      setSticky(scrollY > 64)
    }

    window.addEventListener('scroll', onScroll);

    return () => {
      window.removeEventListener('scroll', onScroll);
    }
  }, []);

  const toggleOffcanvas = (e: any) => {
    e.target.blur();
    setOpenOffcanvas(!openOffcanvas)
  }

  const toggleFormSearch = (e: any) => {
    e.target.blur();
    setOpenFormSearch(() => true);
    inputSearchRef.current.focus?.({ preventScroll: true });
  }

  const blurInputSearch = () => {
    setOpenFormSearch(false)
  }

  return (
    <header
      id="headerMain"
      className={"navbar fixed-top z-5" + (sticky ? " shadow-sm nav-stick" : "")}
    >
      <nav className="flex items-center container">
        <a
          href="/"
          className="mr-2 hide-xs"
        >
          <img
            alt="Monito"
            src="/media/img/logo.png"
            className="nav-logo"
          />
        </a>

        <div 
          className={"offcanvas grow" + (openOffcanvas ? " open" : "") + (openFormSearch ? " open-form" : "")}
        >
          <div className="offcanvas-header text-right">
            <button
              type="button"
              className="btn"
              onClick={toggleOffcanvas}
            >
              <Close />
            </button>
          </div>

          <div className="offcanvas-body gap-4">
            <ul className="navbar-nav items-center gap-4 ml-auto">
              <li>
                <a href="#" className="nav-link px-4 fw-700">Home</a>
              </li>
              <li>
                <a href="#" className="nav-link px-4 fw-700">Category</a>
              </li>
              <li>
                <a href="#" className="nav-link px-4 fw-700">About</a>
              </li>
              <li>
                <a href="#" className="nav-link px-4 fw-700">Contact</a>
              </li>
            </ul>

            <form 
              className={"input-group rounded-pill focus-ring w-280px" + (openFormSearch ? " open" : "")} 
              tabIndex={-1}
            >
              <label htmlFor="searchMain" className="input-group-text pr-0">
                <Search />
              </label>
              <input 
                ref={inputSearchRef}
                id="searchMain"
                type="search"
                className="form-control shadow-none"
                placeholder="Search something here!" 
                onBlur={blurInputSearch}
              />
            </form>

            <button 
              type="button"
              className="btn btn-primary rounded-pill fw-700 btn-join"
            >
              Join the community
            </button>

            <div>
              <button
                type="button"
                className="btn flex items-center btn-profile"
              >
                <StarCircleRed className="mr-2" />
                VND
                <ArrowDown className="ml-2" />
              </button>
            </div>
          </div>
        </div>
      </nav>

      <div className="hide-md flex items-center justify-between">
        <button
          type="button"
          className="btn flex"
          onClick={toggleOffcanvas}
        >
          <svg width="22" height="17" viewBox="0 0 22 17" fill="none">
            <path
              stroke="#00171F" 
              strokeWidth="2" 
              strokeLinecap="round" 
              strokeLinejoin="round"
              d="M1.66675 15.1667H20.3334M1.66675 8.50001H20.3334M1.66675 1.83334H20.3334"
            />
          </svg>
        </button>

        <a
          href="/"
        >
          <img
            alt="Monito"
            src="/media/img/logo.png"
            className="nav-logo"
          />
        </a>

        <button
          type="button"
          className="btn flex"
          onClick={toggleFormSearch}
        >
          <Search />
        </button>
      </div>
    </header>
  );
}
