import { ArrowRight } from '@/components/icons/ArrowRight';

type IPet = {
  id: string;
  code: string;
  name: string;
  gene: string;
  age: string;
  price: string;
  thumbnail: string;
}

export const CardPets = () => {
  const PETS: Array<IPet> = [
    {
      id: "1",
      code: "MO231",
      name: "Pomeranian White",
      gene: "Male",
      age: "02 months",
      price: "6.900.000 VND",
      thumbnail: "/media/img/pet_1.png"
    },
    {
      id: "2",
      code: "MO502",
      name: "Poodle Tiny Yellow",
      gene: "Female",
      age: "02 months",
      price: "3.900.000 VND",
      thumbnail: "/media/img/pet_2.png"
    },
    {
      id: "3",
      code: "MO102",
      name: "Poodle Tiny Sepia",
      gene: "Male",
      age: "02 months",
      price: "4.000.000 VND",
      thumbnail: "/media/img/pet_3.png"
    },
    {
      id: "4",
      code: "MO512",
      name: "Alaskan Malamute Grey",
      gene: "Male",
      age: "02 months",
      price: "8.900.000 VND",
      thumbnail: "/media/img/pet_4.png"
    },
    {
      id: "5",
      code: "MO231",
      name: "Pembroke Corgi Cream",
      gene: "Male",
      age: "02 months",
      price: "7.900.000 VND",
      thumbnail: "/media/img/pet_5.png"
    },
    {
      id: "6",
      code: "MO502",
      name: "Pembroke Corgi Tricolor",
      gene: "Female",
      age: "02 months",
      price: "9.000.000 VND",
      thumbnail: "/media/img/pet_6.png"
    },
    {
      id: "7",
      code: "MO231",
      name: "Pomeranian White",
      gene: "Female",
      age: "02 months",
      price: "6.500.000 VND",
      thumbnail: "/media/img/pet_7.png"
    },
    {
      id: "8",
      code: "MO512",
      name: "Poodle Tiny Dairy Cow",
      gene: "Mmale",
      age: "02 months",
      price: "5.000.000 VND",
      thumbnail: "/media/img/pet_8.png"
    },
  ];

  return (
    <section className="pt-8">
      <div className="container">
        <div className="flex flex-wrap items-center my-6 px-3">
          <h1 className="text-2xl fw-700">
            <small className="block text-base fw-500">Whats new?</small>
            Take a look at some of our pets
          </h1>

          <a href="#" className="btn btn-outline-primary rounded-pill ml-auto hide-xs">
            View more
            <ArrowRight className="ml-2" />
          </a>
        </div>

        <div className="row gap-y-4">
          {PETS.map((item: IPet) =>
            <article key={item.id} className="col-xs-6 col-md-3">
              <div className="card h-full shadow-sm">
                <a href="#" className="no-underline">
                  <img
                    loading="lazy"
                    decoding="async"
                    alt={item.name}
                    src={item.thumbnail}
                    className="w-full rounded-10px"
                  />
                </a>
                <div className="card-body">
                  <h1 className="text-base mb-0">
                    <a href="#" className="no-underline text-dark-1 fw-700">{item.code} - {item.name}</a>
                  </h1>

                  <div className="flex flex-col flex-row-md my-1 text-xs text-gray">
                    <div>
                      Gene: <b>{item.gene}</b>
                    </div>

                    <div className="mx-2 hide-xs">•</div>
                    
                    <div>
                      Age: <b>{item.age}</b>
                    </div>
                  </div>
                  
                  <strong className="text-sm">{item.price}</strong>
                </div>
              </div>
            </article>
          )}
        </div>

        <div className="px-3 hide-md">
          <a href="#" className="btn btn-outline-primary rounded-pill w-full mt-4">
            View more
            <ArrowRight className="ml-2" />
          </a>
        </div>
      </div>
    </section>
  );
}
