import { PlayCircle } from '@/components/icons/PlayCircle';

export const Banner = () => {
  return (
    <section className="banner flex pt-8 relative">
      <div className="rect-top pe-none" />
      <div className="rect-top rect-bottom pe-none" />

      <div className="container mt-auto relative">
        <div className="row relative z-3">
          <div className="col-12 col-md-6 pt-8">
            <h1 className="fw-800 text-primary">
              One more friend
              <small className="fw-700">Thousands more fun!</small>
            </h1>
            <p className="my-6 fw-500">
              Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!
            </p>

            <a href="#" className="btn btn-outline-primary rounded-pill mr-2">
              View Intro
              <PlayCircle className="ml-2" />
            </a>
            <a href="#" className="btn btn-primary rounded-pill ml-2">
              Explore Now
            </a>
          </div>

          <div className="col-12 col-md-6 pt-4-xs mx-auto">
            <img
              loading="lazy"
              decoding="async"
              alt="With dog"
              src="/media/img/with_dog.png"
              className="relative z-3"
            />
          </div>          
        </div>

        <svg className="rect rect-2 z-1 pe-none" width="702" height="413" viewBox="0 0 702 413" fill="none">
          <rect x="89.2182" y="-14" width="635" height="635" rx="99" transform="rotate(9.35484 89.2182 -14)" fill="#003459"/>
        </svg>

        <svg className="rect rect-1 z-2 pe-none" width="781" height="483" viewBox="0 0 781 483" fill="none">
          <rect x="238.67" y="-32" width="635" height="635" rx="99" transform="rotate(25.23 238.67 -32)" fill="#F7DBA7"/>
        </svg>
      </div>
    </section>
  );
}
