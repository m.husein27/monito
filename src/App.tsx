import { useEffect } from 'react';
import { Header } from '@/components/layout/Header';
import { Footer } from '@/components/layout/Footer';
import { Banner } from '@/components/Banner';
import { CardPets } from '@/components/CardPets';
import { BannerSecond } from '@/components/BannerSecond';
import { CardArticles } from '@/components/CardArticles';

export default function App(){
  // Hide loading screen
  useEffect(() => {
    const splashScreen = document.getElementById('_splashScreen') as HTMLDivElement;
    if(splashScreen){
      splashScreen.hidden = true;
    }
  }, []);

  return (
    <>
      <Header />

      <Banner />

      <CardPets />

      <BannerSecond />

      <CardArticles />

      <Footer />
    </>
  );
}
